< envPaths

errlogInit(20000)

dbLoadDatabase("$(ANDORAPP)/dbd/iocApp.dbd")
iocApp_registerRecordDeviceDriver(pdbbase)

epicsEnvSet("PREFIX", "LUCA:")
epicsEnvSet("PORT",   "ANDOR")
epicsEnvSet("QSIZE",  "20")
epicsEnvSet("XSIZE",  "2048")
epicsEnvSet("YSIZE",  "2048")
epicsEnvSet("NCHANS", "2048")
epicsEnvSet("CBUFFS", "500")
epicsEnvSet("MAX_THREADS", "8")
epicsEnvSet("EPICS_DB_INCLUDE_PATH", "$(TOP)/db:$(AUTOSAVE)/db:$(ADCORE)/db:$(ADANDOR)/db:$(ANDORAPP)/db")
epicsEnvSet("EPICS_CA_MAX_ARRAY_BYTES", 10000000)

# andorCCDConfig(const char *portName, const char *installPath, int cameraSerial, int shamrockID,
#                int maxBuffers, size_t maxMemory, int priority, int stackSize)
andorCCDConfig("$(PORT)", "$(ADANDOR)/etc/andor", 1370, 0, 0, 0, 0)

dbLoadRecords("andorCCD.template",   "P=$(PREFIX),R=cam1:,PORT=$(PORT),ADDR=0,TIMEOUT=1")

# Comment out the following lines if there is no Shamrock spectrograph
#shamrockConfig(const char *portName, int shamrockId, const char *iniPath, int priority, int stackSize)
#shamrockConfig("SR1", 0, "", 0, 0)
#dbLoadRecords("$(ADANDOR)/db/shamrock.template",   "P=$(PREFIX),R=sham1:,PORT=SR1,TIMEOUT=1,PIXELS=1024")

# Create a standard arrays plugin
NDStdArraysConfigure("Image1", 5, 0, "$(PORT)", 0, 0)
# Make NELEMENTS in the following be a little bigger than 2048*2048
# Use the following command for 16-bit images.  This can be used for 16-bit detector as long as accumulate mode would not result in 16-bit overflow
dbLoadRecords("NDStdArrays.template", "P=$(PREFIX),R=image1:,PORT=Image1,ADDR=0,TIMEOUT=1,NDARRAY_PORT=$(PORT),TYPE=Int16,FTVL=SHORT,NELEMENTS=4200000")

set_requestfile_path("./")
set_requestfile_path("$(TOP)/req")
set_requestfile_path("$(ANDORAPP)/req")
set_requestfile_path("$(ADANDOR)/req")
set_requestfile_path("$(ADCORE)/req")
set_requestfile_path("$(CALC)/req")
set_savefile_path("../autosave")
set_pass0_restoreFile("auto_settings.sav")
set_pass1_restoreFile("auto_settings.sav")
save_restoreSet_status_prefix("$(PREFIX)")
dbLoadRecords("save_restoreStatus.db", "P=$(PREFIX)")

# Load all other plugins using commonPlugins.cmd
< commonPlugins.cmd

#asynSetTraceIOMask("$(PORT)",0,2)
#asynSetTraceMask("$(PORT)",0,255)

iocInit()

# save things every thirty seconds
create_monitor_set("auto_settings.req", 30, "P=$(PREFIX)")
